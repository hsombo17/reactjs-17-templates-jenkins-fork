import { BrowserRouter as BigRouter, Route, Routes } from 'react-router-dom';
import './App.css';
import HomePage from './pages/HomePage';
import CustomNavBar from './components/CustomNavBar';
import NotFoundPage from './pages/NotFoundPage';
import Service from './pages/Service';
import AboutUs from './pages/AboutUs';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'react-toastify/dist/ReactToastify.css';
import Products from './pages/Products';
import ProductDetail from './pages/ProductDetail';
import ProductDashBoard from './pages/admin/ProductDashBoard';
import Dashboard from './pages/admin/Dashboard';
import UserDashboard from './pages/admin/UserDashboard';
import { useState } from 'react';
import Layout from './components/routes/Layout';
import { ToastContainer } from 'react-toastify';
function App() {
  const [isValidated, setIsValidated] = useState(localStorage.getItem("isLogin") || false  )


  const handleLoginCallBack = (value) => {
    setIsValidated(value) // get value from navbar 
  }
  return (
    <BigRouter>
      <CustomNavBar
        isAuthenticated={isValidated}
        loginCallBack={handleLoginCallBack}
      />
      <Routes>

        {!isValidated ?
          <>
            <Route index
              element={
                <Layout includeFooter={true}>
                  <HomePage />
                </Layout>
              } />

            <Route path="/service"
              element={
                <Layout includeFooter={true}>
                  <Service />
                </Layout>} />
            <Route path="/aboutus"
              element={
                <Layout includeFooter={true}>
                  <AboutUs />
                </Layout>
              } />
            <Route path="/products" element={<Products />} />
            {/* /products/12 */}
            <Route path="/products/:id" element={<ProductDetail />} />
          </>
          :
          <>
            {/* these are for the admin page  */}
            <Route path='/admin/products' element={<ProductDashBoard />} />
            <Route index element={<Dashboard />} />
            <Route path='/admin/users' element={<UserDashboard />} />
          </>
        }
        <Route path="*" element={<NotFoundPage />} />

      </Routes>

      <ToastContainer />
    </BigRouter>
  );
}

export default App;
